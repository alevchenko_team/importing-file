window["ENTITY_SET_NAMES"] = window["ENTITY_SET_NAMES"] || JSON.stringify({
    "account" : "accounts",
    "contact" : "contacts"
  });

function parseFile(){    
    var dataFile =document.getElementById("importFile").files[0];
    Papa.parse(dataFile,{
        header: true,
        skipEmptyLines: true,
        complete: function(results){
            var entityList = results.data;
            return entityList;
            }
    })    
}

function runImport(){
    var entityList = parseFile();
    Import(entityList);
}

