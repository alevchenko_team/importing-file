function Import(id) {
    var contact = retrieveContact(id);
    console.log(contact);
}

function retrieveContact(ContactId) {
    SDK.JQuery.retrieveRecord(
        ContactId,
        "Contact",
        null, null,
        function (contact) {
         writeMessage("Retrieved the contact named \"" + contact.Name + "\". This contact was created on : \"" + contact.CreatedOn + "\".");
         updateContact(ContactId);
        },
        errorHandler
    );
}

function errorHandler(error) {
    console.log(error.message);
}
